[Advent of Code 2016](http://adventofcode.com/2016)
                        
                        *                            
                        |                            
                      +-|---+                        
                     /  |  /|                        
                    +-----+ |                        
                    |:::::| |                        
            +----+  |:::::| |---+      +-----------+ 
           /    / \ |:::::| |  /|     / \\\\\\ [] /| 
          /    / / \|:::::| | / |    / \\\\\\ [] / | 
         /    / / / \:::::|/ /  |   +-----------+  | 
        +----+ / / / \------+ ------|:::::::::::|  | 
        |-----\ / / / \=====| ------|:::::::::::|  | 
        |------\ / / / \====|   |   |:::::::::::|  | 
        |-------\ / / / +===|   |   |:::::::::::|  | 
        |--------\ / / /|===|   |   |:::::::::::|  | 
        |---------\ / / |===|   |  /|:::::::::::|  | 
        |----------\ /  |===|  /  //|:::::::::::| /  
        +-----------+   |===| /  //||:::::::::::|/   
        |:::::::::::|   |===|/__//___________________
        |:::::::::::|   |______//|_____...._________ 
        |:::::::::::|   |     //| ____/ /_/___       
     ---|:::::::::::|   |--------|[][]|_|[][]_\------
    ----|:::::::::::|   |--------------------------- 
     || |:::::::::::|   |  //| ||  / / / ||      ||  
     || |:::::::::::|   | //|  || /   /  ||      ||  
        |:::::::::::|   |//|     / / /               
        |:::::::::::|   //|     /   /   ____________ 
        |:::::::::::|  //|     / / /___/ /#/ /#/#/ / 
        |:::::::::::| //|     /    ___            /  
        |:::::::::::|//|     / / /   /_/_/_/#/#/#/   
      ==============//======+...+====================
      - - - - - - -// - - -/   / - - - - - - - - - - 
    ==============//|==============================  
                 //|                                 


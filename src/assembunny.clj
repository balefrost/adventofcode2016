(ns assembunny
  (:require [clojure.string :as str])
  (:use advent-util))

(defn -make-reg [s]
  (condp = s
    "a" :a
    "b" :b
    "c" :c
    "d" :d
    nil))

(defn -make-arg [s]
  (if-let [r (-make-reg s)]
    r
    (parse-int s)))

(defn -make-instr [line]
  (let [[a b c d] (str/split line #" ")]
    (condp = a
      "cpy" {:opcode :cpy :args [(-make-arg b) (-make-reg c)]}
      "jnz" {:opcode :jnz :args [(-make-arg b) (-make-arg c)]}
      "inc" {:opcode :inc :args [(-make-reg b)]}
      "dec" {:opcode :dec :args [(-make-reg b)]}
      "tgl" {:opcode :tgl :args [(-make-arg b)]}
      "mlt" {:opcode :mlt :args [(-make-arg b) (-make-arg c) (-make-reg d)]}
      "out" {:opcode :out :args [(-make-arg b)]})))

(defn -resolve-arg [state arg]
  (if (keyword? arg)
    ((:registers state) arg)
    arg))

(defn -do-copy [state args]
  (let [registers (:registers state)
        [src dst] args
        value (-resolve-arg state src)]
    (assoc state :registers (assoc registers dst value))))

(defn -do-jnz [state args]
  (let [{ip :ip registers :registers} state
        [src offset] args
        src (-resolve-arg state src)
        offset (-resolve-arg state offset)]
    (if (= 0 src)
      state
      (assoc state :ip (dec (+ ip offset))))))              ;accounts for automatic IP inc

(defn -do-inc [state args]
  (let [registers (:registers state)]
    (assoc state :registers (update registers (first args) inc))))

(defn -do-dec [state args]
  (let [{registers :registers} state]
    (assoc state :registers (update registers (first args) dec))))

(defn -do-tgl [state args]
  (let [{ip :ip instructions :instructions} state
        offset (-resolve-arg state (first args))
        target-instr-idx (+ ip offset)
        target-instr (nth instructions target-instr-idx nil)]
    (if target-instr
      (let [new-opcode (case (:opcode target-instr)
                         :cpy :jnz
                         :jnz :cpy
                         :inc :dec
                         :dec :inc
                         :tgl :inc)
            new-instr (assoc target-instr :opcode new-opcode)]
        (assoc state :instructions (assoc instructions target-instr-idx new-instr)))
      state)))

(defn -do-mult [state args]
  (let [registers (:registers state)
        [a b dst] args
        a (-resolve-arg state a)
        b (-resolve-arg state b)]
    (assoc-in state [:registers dst] (* a b))))

(defn -do-out [state args]
  (let [output (:output state)
        [a] args
        a (-resolve-arg state a)]
    (update state :output conj a)))

(defn tick [state]
  (let [{instructions :instructions ip :ip} state]
    (if-let [instr (nth instructions ip nil)]
      (let [f (case (:opcode instr)
                :cpy -do-copy
                :jnz -do-jnz
                :inc -do-inc
                :dec -do-dec
                :tgl -do-tgl
                :mlt -do-mult
                :out -do-out)]
        (-> state
            (f (:args instr))
            (update :ip inc)
            (update-in [:profile ip] inc))))))

(defn initial-state
  ([instructions]
   (initial-state instructions {}))
  ([instructions registers]
   {:ip           0
    :instructions (vec instructions)
    :profile      (vec (repeat (count instructions) 0))
    :registers    (merge {:a 0 :b 0 :c 0 :d 0} registers)
    :output       []}))

(defn -ignore-line? [line]
  (let [trimmed (.trim line)]
    (or
      (.isEmpty trimmed)
      (.startsWith trimmed ";"))))

(defn parse-program [program-text]
  (map -make-instr (remove -ignore-line? (str/split-lines program-text))))

(defn run-program
  ([program]
   (run-program program {}))
  ([program registers]
   (let [s (initial-state program registers)]
     (:registers (last (take-while identity (iterate tick s)))))))

(defn trace-program
  ([program]
   (run-program program {}))
  ([program registers]
   (let [s (initial-state program registers)]
     (take-while identity (iterate tick s)))))

(defn display-profile [state]
  (let [{instructions :instructions
         profile      :profile} state
        blank-ip-vec (vec (repeat (count instructions) " "))
        ip (:ip state)
        current-ip-vec (if (and (>= ip 0) (< ip (count blank-ip-vec)))
                         (assoc blank-ip-vec (:ip state) "*")
                         blank-ip-vec)
        largest-profile-count (apply max profile)
        largest-profile-power (if (= 0 largest-profile-count)
                                1
                                (inc (int (Math/floor (Math/log10 largest-profile-count)))))
        format-string (str "%" largest-profile-power "d")
        profile-vector (map #(format format-string %) profile)
        ip-and-profile-vector (map #(str %1 "  " %2) current-ip-vec profile-vector)]
    (-> state
        (assoc :instructions (map vector ip-and-profile-vector instructions))
        (dissoc :profile))))
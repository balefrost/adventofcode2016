(ns day1
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (slurp (io/resource "day1_input.txt")))

(def orientations
  [[0 1]
   [1 0]
   [0 -1]
   [-1 0]])

(defn make-cmd [str]
  {:turn (nth str 0) :distance (parse-int (subs str 1))})

(def input-cmds (map make-cmd (str/split input-string #", ")))

(defn add-position [curr delta]
  (map + curr delta))

(defn walk [ { pos :pos orient :orient } cmd ]
  (let [delta-o ({\L -1 \R 1} (:turn cmd))
        new-o (mod (+ orient delta-o) 4)
        steps (repeat (:distance cmd) (orientations new-o))
        locations (reductions add-position pos steps)]
    (map (fn [l] { :pos l :orient new-o }) locations)))

(defn path [state cmds]
  (map :pos (flatten (reductions #(drop 1 (walk (last %1) %2)) [state] cmds))))

(defn abs [n]
  (max n (- n)))

(defn manhattan-distance [pos1 pos2]
  (reduce + (map (comp abs -) pos2 pos1)))

(def init-state { :pos [0 0] :orient 0})

(defn part1 []
  (let [end (last (path init-state input-cmds))]
    (str "total distance: " (manhattan-distance [0 0] end))))

(defn part2 []
  (let [thepath (path init-state input-cmds)
        visited (reductions conj #{} thepath)
        pairs (map vector visited thepath)
        previously-visited-pairs (filter #(contains? (first %) (second %)) pairs)
        previously-visited (map second previously-visited-pairs)]
    (manhattan-distance [0 0] (first previously-visited))))

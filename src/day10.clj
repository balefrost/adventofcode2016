(ns day10
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (slurp (io/resource "day10_input.txt")))

(defn parse-destination [destination]
  (let [[type number] destination]
    (condp = type
      "bot" {:type   :bot
             :number (parse-int number)}
      "output" {:type   :output
                :number (parse-int number)})))

(defn parse-command [line]
  (let [[command & tokens] (str/split line #" ")]
    (condp = command
      "value" (let [[value zgoes zto & destination] tokens]
                (assert (and (= zgoes "goes") (= zto "to")))
                {:type        :init
                 :value       (parse-int value)
                 :destination (parse-destination destination)})
      "bot" (let [[botnumber zgives zlow zto1 lowtype lownumber zand zhigh zto2 hightype highnumber] tokens]
              (assert (and
                        (= zgives "gives")
                        (= zlow "low")
                        (= zto1 "to")
                        (= zand "and")
                        (= zhigh "high")
                        (= zto2 "to")))
              {:type      :give
               :botnumber (parse-int botnumber)
               :low       (parse-destination [lowtype lownumber])
               :high      (parse-destination [hightype highnumber])}))))

(def all-directives (group-by :type (map parse-command (str/split-lines input-string))))

(def init-directives (:init all-directives))
(def give-directives (into {} (map (fn [{botnumber :botnumber :as d}] [botnumber d]) (:give all-directives))))

(defn execute-give [state cmd]
  (let [{bots :bots outputs :outputs} state
        {destination :destination value :value} cmd]
    (condp = (:type destination)
      :bot {:bots    (update bots (:number destination) (comp sort (fnil conj [])) value)
            :outputs outputs}
      :output {:bots    bots
               :outputs (update outputs (:number destination) (fnil conj []) value)})))

(def initial-state (reduce execute-give {:bots {} :outputs {}} init-directives))

(defn make-give-commands [bot-state]
  (let [[botnumber [lowvalue highvalue]] bot-state
        {low-dest :low high-dest :high} (get give-directives botnumber)]
    [{:value lowvalue :destination low-dest}
     {:value highvalue :destination high-dest}]))

(defn next-state [current-state]
  (let [{ bots :bots outputs :outputs } current-state
        bots-with-two-values (filter #(= 2 (count (second %))) bots)
        bots-with-more-than-two-values (filter #(> (count (second %)) 2) bots)
        bots (reduce dissoc bots (keys bots-with-two-values))
        current-state { :bots bots :outputs outputs}
        give-commands (mapcat make-give-commands bots-with-two-values)]
    (assert (= 0 (count bots-with-more-than-two-values)))
    (if (empty? bots-with-two-values)
      nil
      (reduce execute-give current-state give-commands))))

(defn states [state]
  (take-while identity (iterate next-state state)))

(defn find-bot-comparing [low high]
  (fn [{bots :bots }]
    (first (filter (fn [[botnumber values]] (= values [low high])) bots))))

(defn part1 []
  (first (mapcat (find-bot-comparing 17 61) (states initial-state))))

(defn part2 []
  (let [final-state (last (states initial-state))
        outputs (:outputs final-state)
        values (map #(first (get outputs %)) [0 1 2])]
    (apply * values)))

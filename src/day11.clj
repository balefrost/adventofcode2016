(ns day11
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(defrecord State [elevator slots])

(defn compare-states [a b]
  (compare [(:g a) (:m a)] [(:g b) (:m b)]))

(defn normalize-state [state]
  (assoc state :slots (vec (sort compare-states (:slots state)))))

(def initial-state-part1
  (normalize-state
    (State. 0 [{:g 0 :m 1}
               {:g 0 :m 0}
               {:g 0 :m 1}
               {:g 0 :m 0}
               {:g 0 :m 0}])))

(def initial-state-part2
  (normalize-state
    (State. 0 [{:g 0 :m 1}
               {:g 0 :m 0}
               {:g 0 :m 1}
               {:g 0 :m 0}
               {:g 0 :m 0}
               {:g 0 :m 0}
               {:g 0 :m 0}])))

(defn get-floor [state floor-number]
  (let [slots (:slots state)
        generators (set (map #(if (= (:g %1) floor-number) {:s %2 :k :g}) slots (range)))
        microchips (set (map #(if (= (:m %1) floor-number) {:s %2 :k :m}) slots (range)))]
    (remove nil? (concat generators microchips))))

(defn get-floors [state]
  (map #(get-floor state %) (range 0 4)))

(defn is-valid-floor? [floor]
  (let [generators (into #{} (map :s (filter #(= (:k %) :g) floor)))
        microchips (into #{} (map :s (filter #(= (:k %) :m) floor)))
        bare-chips (remove generators microchips)]
    (or
      (empty? bare-chips)
      (empty? generators))))

(defn is-valid-state? [state]
  (every? is-valid-floor? (get-floors state)))

(defn -combinations-helper [coll n acc]
  (lazy-seq
    (if (= n 0)
      [acc]
      (if (empty? coll)
        nil
        (concat
          (-combinations-helper (rest coll)
                                (dec n)
                                (conj acc (first coll)))
          (-combinations-helper (rest coll)
                                n
                                acc))))))

(defn combinations [coll n]
  (-combinations-helper coll n []))

(defn is-done? [state]
  (every? empty? (drop-last (get-floors state))))

(defn move-item-to-floor [slots item destination-floor-number]
  (assoc-in slots [(:s item) (:k item)] destination-floor-number))

(defn move-payload-to-floor [slots payload destination-floor-number]
  (reduce #(move-item-to-floor %1 %2 destination-floor-number) slots payload))

(defn next-states [state]
  (let [current-floor-number (:elevator state)
        current-floor (get-floor state current-floor-number)
        possible-payload (concat (combinations current-floor 2) (map vector current-floor))
        possible-destination-floor-numbers ([[1] [0 2] [1 3] [2]] current-floor-number)
        possible-moves (for [payload possible-payload
                             destination-floor-number possible-destination-floor-numbers]
                         (let [slots (move-payload-to-floor (:slots state) payload destination-floor-number)]
                           (normalize-state (->State destination-floor-number slots))))
        valid-moves (into #{} (filter is-valid-state? possible-moves))]
    valid-moves))

(defn breadth-first-search-count [pred step init-states]
  (loop [curr-depth 0
         curr-states (set init-states)]
    (println "search space" (count curr-states))
    (if (some pred curr-states)
      curr-depth
      (recur (inc curr-depth) (into #{} (mapcat identity (pmap next-states curr-states)))))))

(defn part1 []
  (breadth-first-search-count is-done? next-states [initial-state-part1]))

(defn part2 []
  (breadth-first-search-count is-done? next-states [initial-state-part2]))
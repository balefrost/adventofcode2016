(ns day12
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [assembunny :as asm])
  (:use advent-util))

(def input-string (slurp (io/resource "day12_input.txt")))

(def input-program (asm/parse-program input-string))

(defn part1 []
  (let [s (asm/initial-state input-program)]
    (get-in (last (take-while identity (iterate asm/tick s)))
            [:registers :a])))

(defn part2 []
  (let [s (assoc-in (asm/initial-state input-program) [:registers :c] 1)]
    (get-in (last (take-while identity (iterate asm/tick s)))
            [:registers :a])))
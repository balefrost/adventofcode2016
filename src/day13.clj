(ns day13
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.set :as set]))

(def input-number 1358)

(defn even-number-of-one-bits? [^long v]
  (let [b (bit-xor (unsigned-bit-shift-right v 32) (bit-and v 0xffffffff))
        c (bit-xor (unsigned-bit-shift-right v 16) (bit-and v 0xffff))
        d (bit-xor (unsigned-bit-shift-right c 8) (bit-and c 0xff))
        e (bit-xor (unsigned-bit-shift-right d 4) (bit-and d 0x0f))
        f (bit-xor (unsigned-bit-shift-right e 2) (bit-and e 0x03))
        g (bit-xor (unsigned-bit-shift-right f 1) (bit-and f 0x01))]
    (= g 0)))

(defn is-open? [location]
  (let [[x y] location
        a (+ (* x x) (* 3 x) (* 2 x y) y (* y y))
        b (+ a input-number)
        c (even-number-of-one-bits? b)]
    c))

(defn indoor-location? [location]
  (let [[x y] location]
    (and (>= x 0) (>= y 0))))

(def valid-location? (every-pred indoor-location? is-open?))

(defn adjacent-locations [location]
  (map #(map + %1 %2) [[0 1] [1 0] [0 -1] [-1 0]] (repeat location)))

(defn next-state [state]
  (let [{visited :visited fringe :fringe distance :distance} state
        valid-adjacent (->> fringe
                            (mapcat adjacent-locations)
                            (filter valid-location?)
                            (remove visited)
                            (set))]
    {:visited  (set/union visited valid-adjacent)
     :fringe   valid-adjacent
     :distance (inc distance)}))

(def initial-state {:visited  #{[1 1]}
                    :fringe   #{[1 1]}
                    :distance 0})

(defn part1 []
  (:distance (first (drop-while #(not ((:fringe %) [31 39])) (iterate next-state initial-state)))))

(defn part2 []
  (count (:visited (nth (iterate next-state initial-state) 50))))
(ns day14
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.set :as set])
  (:use advent-util)
  (:import (java.security MessageDigest)))

(def input-value "yjdafjpo")

(defn compute-md5-string-2017-times [^String string]
  (nth (iterate compute-md5-string string) 2017))

(defn tripled [string]
  (if-let [s (re-seq #"(.)\1\1" string)]
    (map #(.charAt ^String (second %) 0) s)))

(defn quinted [string]
  (if-let [s (re-seq #"(.)\1\1\1\1" string)]
    (map #(.charAt ^String (second %) 0) s)))

(defn make-iteration-processor [f input-value]
  (fn [value]
    (let [md5string (f (str input-value value))
          triples (tripled md5string)
          quints (quinted md5string)]
      {:index  value
       :triple (first triples)
       :quints (set quints)})))

(defn process-partition [partition]
  (let [[hd & tl] partition
        triple (:triple hd)]
    (if (and
          (not (nil? triple))
          (some #(% triple) (map :quints tl)))
      (:index hd))))

(defn find-answer [f input-value n]
  (let [input-values (pmap (make-iteration-processor f input-value) (range))
        partitions (partition 1001 1 input-values)
        filtered-partitions (pmap process-partition partitions)
        surviving-partitions (remove nil? filtered-partitions)]
    (nth surviving-partitions n)))

(defn part1 []
  (find-answer compute-md5-string input-value 63))

(defn part2 []
  (find-answer compute-md5-string-2017-times input-value 63))

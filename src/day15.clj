(ns day15
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.set :as set])
  (:use advent-util))

(def input-lines (str/split-lines (slurp (io/resource "day15_input.txt"))))

(defn parse-disc [str]
  (let [re #"Disc #(\d+) has (\d+) positions; at time=0, it is at position (\d+)."
        groups (rest (re-matches re str))
        [number positions init-position] (map parse-int groups)]
    {:number        number
     :positions     positions
     :init-position init-position}))

; disc 1 time: ta = tdrop + 1
; disc 1 position: (10 + ta) % 13
;           solve: (10 + ta) % 13 = 0
;                  11 + tdrop = 13 * a
;                  13 * a - tdrop = 11
; disc 2 time: tb = tdrop + 2
; disc 2 position: (15 + tb) % 17
;           solve: (15 + tb) % 17 = 0
;                  17 + tdrop = 17 * b
;                  17 * b - tdrop = 17

(def input-discs (map parse-disc input-lines))

(defn make-disc-predicate [discs]
  (fn [time]
    (loop [discs discs]
      (if (empty? discs)
        time
        (let [[disc & discs] discs
              {number :number positions :positions init-position :init-position} disc]
          (if (= 0 (mod (+ number init-position time) positions))
            (recur discs)
            nil))))))

(defn solve [discs]
  (first (remove nil? (pmap (make-disc-predicate discs) (range)))))

(defn part1 []
  (solve input-discs))

;3730820 too high
(defn part2 []
  (solve (concat input-discs [{:number        (inc (count input-discs))
                               :positions     11
                               :init-position 0}])))
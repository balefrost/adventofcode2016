(ns day16
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.set :as set])
  (:use advent-util))

(defn parse-dragon-string [str]
  (mapv #(condp = %
           \0 (byte 0)
           \1 (byte 1)) str))

(defn invert-bit [bit]
  (condp = bit
    0 (byte 1)
    1 (byte 0)))

(defn conj-dragon-tail! [current]
  (loop [current current
         x (- (count current) 2)]
    (if (>= x 0)
      (-> current
        (conj! (invert-bit (nth current x)))
        (recur (dec x)))
      current)))

(defn dragon-string [seed length]
  (loop [current (transient (vec seed))]
    (if (>= (count current) length)
      (take length (persistent! current))
      (-> current
          (conj! 0)
          (conj-dragon-tail!)
          (recur)))))

(defn compute-same-bits [dragon-string]
  (let [length (count dragon-string)]
    (loop [current (transient [])
           i 0]
      (if (< i length)
        (let [a (nth dragon-string i)
              b (nth dragon-string (inc i))
              c (if (= a b) (byte 1) (byte 0))]
          (-> current
              (conj! c)
              (recur (+ i 2))))
        (persistent! current)))))

(defn compute-checksum [dragon-string]
  (loop [current (vec dragon-string)]
    (if (odd? (count current))
      current
      (recur (compute-same-bits current)))))

(def init-string (parse-dragon-string "10111100110001111"))

(defn solve [length]
  (let [checksum (compute-checksum (dragon-string init-string length))]
    (str/join checksum)))

(defn part1 []
  (solve 272))

(defn part2 []
  (solve 35651584))


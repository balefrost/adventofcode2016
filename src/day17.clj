(ns day17
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.set :as set])
  (:use advent-util))

(defrecord path [trail location])
(defrecord state [paths depth])

(defn compute-4-char-md5-string [^String string]
  (bytes-to-hex (compute-md5 (.getBytes string "US-ASCII"))
                2))

(defn is-door-open? [char]
  (#{\b \c \d \e \f} char))

(def exits (map vector [\U \D \L \R]))
(def directions {\U [0 -1]
                 \D [0 1]
                 \L [-1 0]
                 \R [1 0]})

(defn possible-exits [trail]
  (let [md5sum (compute-4-char-md5-string trail)
        open-doors (map is-door-open? md5sum)]
    (into #{} (mapcat #(if %1 %2) open-doors exits))))

(defn valid-location? [location]
  (let [[x y] location]
    (and (>= x 0) (< x 4) (>= y 0) (< y 4))))

(defn next-paths [path]
  (let [{trail :trail location :location} path
        exits (possible-exits trail)
        new-paths (map #(->path (str trail %1) (mapv + location (directions %1))) exits)
        valid-paths (filter #(valid-location? (:location %)) new-paths)]
    valid-paths))

(defn next-state [state]
  (let [{paths :paths depth :depth} state
        new-paths (mapcat identity (pmap next-paths paths))]
    (->state new-paths (inc depth))))

(def puzzle-input "vwbaicqe")

(def initial-state (->state [(->path puzzle-input [0 0])] 0))

(defn is-final-location? [location]
  (= location [3 3]))

(defn if-final-state [state]
  (let [paths (:paths state)]
    (some #(if (is-final-location? (:location %))
             state)
          paths)))

(defn part1 []
  (subs (:trail
          (first (filter
                   #(is-final-location? (:location %))
                   (:paths (some if-final-state (iterate next-state initial-state))))))
        (.length puzzle-input)))

(defn remove-successes [state]
  (let [paths (:paths state)]
    (assoc state :paths (remove #(= (:location %) [3 3]) paths))))

(defn part2 []
  (:depth
    (last
      (filter if-final-state
              (iterate #(do
                          (println "depth" (:depth %) "width" (count (:paths %)))
                          (next-state (remove-successes %))) initial-state)))))

(defn dotest []
  (:depth
    (last (filter if-final-state
                  (take-while #(> (count (:paths %)) 0)
                              (iterate #(next-state (remove-successes %)) initial-state))))))

(ns day18
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (slurp (io/resource "day18_input.txt")))

(def initial-row (map #(= \^ %) (str/trim input-string)))

(defmacro xor [a b]
  `(let [x# ~a
        y# ~b]
     (or
       (and x# (not y#))
       (and y# (not x#)))))

(defn next-cell [previous-cells]
  (let [[l c r] previous-cells]
    (xor l r)))

(defn display-row [row]
  (str/join (map #(if % \^ \.)
                 row)))

(defn next-row [previous-row]
  (let [augrow (concat [false] previous-row [false])]
    (map next-cell (partition 3 1 augrow))))

(defn count-traps-in-row [row rowid]
  (println rowid)
  (count (filter true? row)))

(defn solve [nrows]
  (let [all-rows (iterate next-row initial-row)
        select-rows (take nrows all-rows)
        counts (pmap count-traps-in-row select-rows (range))
        totalcount (reduce + 0 counts)]
    (- (* nrows (count initial-row)) totalcount)))

(defn part1 []
  (solve 40))

(defn part2 []
     (solve 400000))
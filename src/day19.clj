(ns day19
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def puzzle-input 3001330)

(defn take-from-left [state]
  (let [[remaining size] state]
    (cond
      (= size 1) state
      (even? size) [(take-nth 2 remaining) (quot size 2)]
      :else [(drop 1 (take-nth 2 remaining)) (quot size 2)])))

(defn iterate-to-fixpoint [next-fn state]
  (loop [current state]
    (let [next (next-fn current)]
      (if (= current next)
        current
        (recur next)))))

(defn part1 []
  (let [initial-state [(vec (range 1 (inc puzzle-input))) puzzle-input]
        [elements _] (iterate-to-fixpoint take-from-left initial-state)]
    (first elements)))

(defn quot-ceil [a b]
  (let [q (quot a b)
        r (rem a b)]
    (if (= r 0)
      q
      (inc q))))

; so given a sequence [abcd efgh ijkl], we know:
; size is 12; a will steal from g (+6)
; size is 11; b will steal from h (+5 +1)
; size is 10; c will steal from j (+5 +1 +1)
; size is 9; d will steal from k (+4 +1 +1 +1)
; at this point, we can re-assemble the sequence:
;     [efil abcd]
; Note that the first third of entries gets rotated to the end, and we remove 2/3 of
; the remaining items.
(defn take-from-across [remaining]
  (let [size (count remaining)]
    (println size)
    (cond
      (= 1 size) remaining
      (= 2 size) (take 1 remaining)
      :else (let [half-size (quot size 2)
                  third-size (quot-ceil size 3)
                  processed-head (subvec remaining 0 third-size)
                  unprocessed-head (subvec remaining third-size half-size)
                  to-skip (if (even? size) 2 1)
                  tail (take-nth 3 (subvec remaining (+ half-size to-skip)))]
              (into [] (concat unprocessed-head tail processed-head))))))

(defn part2 []
  (let [initial-state (vec (range 1 (inc puzzle-input)))
        result (first (iterate-to-fixpoint take-from-across initial-state))]
    result))
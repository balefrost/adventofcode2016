(ns day2
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(def input-string (slurp (io/resource "day2_input.txt")))

(def input-lines (str/split-lines input-string))

(def directions {\U [0 -1]
                 \R [1 0]
                 \D [0 1]
                 \L [-1 0]})

(def input-directions (map #(map directions %) input-lines))

(defn move [pos-to-button curr-pos delta-pos]
  (let [proposed-pos (map + curr-pos delta-pos)]
    (if (pos-to-button proposed-pos)
      proposed-pos
      curr-pos)))

(defn move-all [pos-to-button curr-pos directions]
  (reduce #(move pos-to-button %1 %2) curr-pos directions))

(defn walk [pos-to-button pos moves]
  (drop 1 (reductions #(move-all pos-to-button %1 %2) pos moves)))

(defn pos-to-9key [curr-pos]
  (if (every? #(and (>= % 0) (<= % 2)) curr-pos)
    (let [[x y] curr-pos]
      (char (+ 48 (+ (* y 3) x))))))

(def pos-to-13key
  { [ 2 0 ] \1
    [ 1 1 ] \2
    [ 2 1 ] \3
    [ 3 1 ] \4
    [ 0 2 ] \5
    [ 1 2 ] \6
    [ 2 2 ] \7
    [ 3 2 ] \8
    [ 4 2 ] \9
    [ 1 3 ] \A
    [ 2 3 ] \B
    [ 3 3 ] \C
    [ 2 4 ] \D
})

(defn part1 []
  (str/join (map pos-to-9key (walk pos-to-9key [1 1] input-directions))))

(defn part2 []
  (str/join (map pos-to-13key (walk pos-to-13key [0 2] input-directions))))

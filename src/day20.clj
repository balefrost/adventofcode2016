(ns day20
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (slurp (io/resource "day20_input.txt")))

(def input-lines (str/split-lines input-string))

(def input-ranges (map #(let [[_ l h] %] [(parse-long l) (parse-long h)])
                       (re-seq #"(\d+)-(\d+)" input-string)))

(defn split-interval [minuend subtrahend]
  (let [[ml mh] minuend
        [sl sh] subtrahend]
    (cond
      (and (<= sl ml) (>= sh mh)) []
      (and (<= sl ml) (< sh mh)) [[(inc sh) mh]]
      (and (> sl ml) (>= sh mh)) [[ml (dec sl)]]
      (and (> sl ml) (< sh mh)) [[ml (dec sl)] [(inc sh) mh]]
      :else [minuend])))

(defn subtract-range [intervalset interval]
  (let [[before rest] (split-with #(< (second %) (first interval)) intervalset)
        [intersecting after] (split-with #(< (first %) (second interval)) rest)]
    (concat
      before
      (mapcat #(split-interval % interval) intersecting)
      after)))

(def max-ip-address (dec (bit-shift-left 1 32)))

(defn part1 []
  (first (first (reduce subtract-range [[0 max-ip-address]] input-ranges))))

(defn part2 []
  (let [results (reduce subtract-range [[0 max-ip-address]] input-ranges)
        interval-sizes (map #(let [[l h] %] (inc (- h l))) results)]
    (reduce + 0 interval-sizes)))


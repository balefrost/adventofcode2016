(ns day21
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (slurp (io/resource "day21_input.txt")))

(def input-lines (str/split-lines input-string))

(defmacro -condr [sym forms]
  (if-let [[[binding-forms r] b & forms] forms]
    `(if-let [~binding-forms (re-matches ~r ~sym)]
       ~b
       (-condr ~sym ~forms))))

(defmacro condr [s & forms]
  `(let [s# ~s]
     (-condr s# ~forms)))

(defn make-command [line]
  (condr line
         ([_ a b] #"swap position (\d+) with position (\d+)") (let [a (parse-int a)
                                                                    b (parse-int b)
                                                                    x (min a b)
                                                                    y (max a b)]
                                                                {:type :swap-positions
                                                                 :a    x
                                                                 :b    y})
         ([_ a b] #"swap letter (.) with letter (.)") {:type :swap-letters
                                                       :a    (first a)
                                                       :b    (first b)}
         ([_ a] #"rotate left (\d+) steps?") {:type  :rotate-left-by-steps
                                              :steps (parse-int a)}
         ([_ a] #"rotate right (\d+) steps?") {:type  :rotate-right-by-steps
                                               :steps (parse-int a)}
         ([_ a] #"rotate based on position of letter (.)") {:type   :rotate-by-letter-position
                                                            :letter (first a)}
         ([_ a b] #"reverse positions (\d+) through (\d+)") {:type      :reverse-range
                                                             :start-pos (parse-int a)
                                                             :stop-pos  (parse-int b)}
         ([_ a b] #"move position (\d+) to position (\d+)") {:type :move-character
                                                             :from (parse-int a)
                                                             :to   (parse-int b)}))

(def input-commands (map make-command input-lines))

(defn do-swap-positions [current a b]
  (let [aval (.charAt current a)
        bval (.charAt current b)
        left (subs current 0 a)
        mid (subs current (inc a) b)
        right (subs current (inc b))]
    (str left bval mid aval right)))

(defn do-swap-letters [current a b]
  (str/join (map #(condp = %
                    a b
                    b a
                    %)
                 current)))

(defn do-rotate-left-by-steps [current steps]
  (let [steps (mod steps (.length current))
        init (subs current 0 steps)
        tail (subs current steps)]
    (str tail init)))

(defn do-rotate-right-by-steps [current steps]
  (let [steps (mod steps (.length current))
        pos (- (.length current) steps)
        init (subs current 0 pos)
        tail (subs current pos)]
    (str tail init)))

(defn do-rotate-by-letter-position [current letter]
  (let [idx (.indexOf current (int letter))
        steps (if (>= idx 4)
                (+ 2 idx)
                (+ 1 idx))]
    (do-rotate-right-by-steps current steps)))

(defn do-rotate-by-letter-position-backward [current letter]
  (assert (= 8 (.length current)))
  (let [steps-left (case (.indexOf current (int letter))
                     0 9
                     1 1
                     2 6
                     3 2
                     4 7
                     5 3
                     6 8
                     7 4)]
    (do-rotate-left-by-steps current steps-left)))

(defn do-reverse-range [current start-pos stop-pos]
  (let [before (subs current 0 start-pos)
        to-reverse (subs current start-pos (inc stop-pos))
        after (subs current (inc stop-pos))]
    (str before (str/reverse to-reverse) after)))

(defn do-move-character [current from to]
  (let [before (subs current 0 from)
        after (subs current (inc from))
        temp (str before after)
        before (subs temp 0 to)
        after (subs temp to)]
    (str before (.charAt current from) after)))

(defn process-command [current cmd]
  (case (:type cmd)
    :swap-positions (do-swap-positions current (:a cmd) (:b cmd))
    :swap-letters (do-swap-letters current (:a cmd) (:b cmd))
    :rotate-left-by-steps (do-rotate-left-by-steps current (:steps cmd))
    :rotate-right-by-steps (do-rotate-right-by-steps current (:steps cmd))
    :rotate-by-letter-position (do-rotate-by-letter-position current (:letter cmd))
    :reverse-range (do-reverse-range current (:start-pos cmd) (:stop-pos cmd))
    :move-character (do-move-character current (:from cmd) (:to cmd))))

(defn process-command-backward [current cmd]
  (case (:type cmd)
    :swap-positions (do-swap-positions current (:a cmd) (:b cmd))
    :swap-letters (do-swap-letters current (:a cmd) (:b cmd))
    :rotate-left-by-steps (do-rotate-right-by-steps current (:steps cmd))
    :rotate-right-by-steps (do-rotate-left-by-steps current (:steps cmd))
    :rotate-by-letter-position (do-rotate-by-letter-position-backward current (:letter cmd))
    :reverse-range (do-reverse-range current (:start-pos cmd) (:stop-pos cmd))
    :move-character (do-move-character current (:to cmd) (:from cmd))))

(defn part1 []
  (reduce process-command "abcdefgh" input-commands))

(defn part2 []
  (reduce process-command-backward "fbgdceah" (reverse input-commands)))

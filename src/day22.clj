(ns day22
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-lines (str/split-lines (slurp (io/resource "day22_input.txt"))))

(def line-pattern #"/dev/grid/node-x(\d+)-y(\d+)\s+(\d+)T\s+(\d+)T\s+(\d+)T\s+(\d+)%")

(defrecord Location [x y])

(defn process-line [line]
  (if-let [[[_ x y size used avail use :as m]] (re-seq line-pattern line)]
    [[(->Location (parse-int x) (parse-int y))
      {:size  (parse-int size)
       :used  (parse-int used)
       :avail (parse-int avail)}]]))

(def input-data (into {} (mapcat process-line input-lines)))

(def all-locations (set (keys input-data)))

(def all-pairs (for [a all-locations
                     b all-locations :when (not= a b)]
                 [a b]))

(defn is-viable-move? [grid from-location to-location]
  (let [a-val (grid from-location)
        b-val (grid to-location)
        a-used (:used a-val)
        b-avail (:avail b-val)]
    (and
      (> a-used 0)
      (< a-used b-avail))))

(defn part1 []
  (count (filter #(is-viable-move? input-data (first %) (second %)) all-pairs)))

(defn visualize-grid-cell [grid goal loc]
  (let [{size :size used :used avail :avail} (grid loc)]
    (cond
      (= loc goal) \G
      (> used 100) \#
      (= used 0) \_
      :else \.)))

(defn visualize-grid-row [grid goal y]
  (if (grid (->Location 0 y))
    (let [locations (map #(->Location % y) (range))
          row-locations (take-while #(grid %) locations)
          row-chars (map #(visualize-grid-cell grid goal %) row-locations)]
      (str/join row-chars))))

(defn visualize-grid [grid goal]
  (str/join \newline
            (take-while (complement nil?) (map #(visualize-grid-row grid goal %) (range)))))

;Super cheaty solution to part2 after visualizing the board:
;
;65 moves to get empty space to the immediate left of the payload
;5 each to move payload one to left and return empty node to left of payload
;    36 such moves leaves the payload at { 1, 0 }
;1 final move
;
;This could instead be done with something like A* (to get the empty node to the goal)
;and then A* with a different heuristic (to move the payload to the left).
(defn part2 []
  (+ 65 (* 5 36) 1))

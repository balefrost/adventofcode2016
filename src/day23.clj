(ns day23
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [assembunny :as asm])
  (:use advent-util))

(def input-string (slurp (io/resource "day23_input.txt")))

(def input-program (asm/parse-program input-string))

(def input-string-alt (slurp (io/resource "day23_alt_input.txt")))

(def input-program-alt (asm/parse-program input-string-alt))

(defn part1 []
  (:a (asm/run-program input-program {:a 7})))

(defn part2 []
  (:a (asm/run-program input-program-alt {:a 12})))

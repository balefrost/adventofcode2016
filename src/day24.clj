(ns day24
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(defrecord Location [x y])

(def input-string (slurp (io/resource "day24_input.txt")))

(defn iterate-maze-text [maze-text]
  (for [[line-number line] (map vector (range) (str/split-lines maze-text))
        [char-number ch] (map vector (range) (str/trim line))]
    (let [loc (->Location char-number line-number)]
      [loc ch])))

(defn parse-maze [maze-text]
  (let [entries (into {} (iterate-maze-text maze-text))]
    {:entries   entries
     :waypoints (into {}
                      (->> entries
                           (filter #(Character/isDigit ^char (second %)))
                           (map (comp vec reverse))))}))

(defn visualize-maze-row [maze row-number]
  (if-let [row-chars (seq (take-while identity (map #(maze (->Location % row-number)) (range))))]
    (str/join row-chars)))

(defn visualize-maze [maze]
  (let [entries (:entries maze)]
    (str/join \newline (take-while identity (map #(visualize-maze-row entries %) (range))))))

(defn println-maze [maze]
  (println (visualize-maze maze)))

(defn adjacent-locations [loc]
  (let [{x :x y :y} loc]
    #{(->Location (dec x) y)
      (->Location (inc x) y)
      (->Location x (dec y))
      (->Location x (inc y))}))

(defn exploration-fn [maze-entries state]
  (let [{fringe   :fringe
         visited  :visited
         distance :distance
         results  :results} state
        distance (inc distance)
        potential-fringe (set (mapcat adjacent-locations fringe))
        unvisited-fringe (clojure.set/difference potential-fringe visited)
        fringe-spaces (set (filter #(or
                                      (= \. (maze-entries %))
                                      (Character/isDigit ^char (maze-entries %)))
                                   unvisited-fringe))
        fringe-goals (set (filter #(Character/isDigit ^char (maze-entries %)) unvisited-fringe))
        new-results (into {} (map #(do [(maze-entries %) distance]) fringe-goals))]
    (assoc state :fringe fringe-spaces
                 :visited (clojure.set/union visited unvisited-fringe)
                 :distance distance
                 :results (into new-results results))))     ;preserves older results

(defn explore-from [maze-entries loc]
  (:results
    (first
      (drop-while
        (comp not-empty :fringe)
        (iterate #(exploration-fn maze-entries %)
                 {:fringe   #{loc}
                  :visited  #{loc}
                  :distance 0
                  :results  {}})))))

(defn find-maze-graph [maze]
  (into {}
        (apply concat (for [[ch loc] (:waypoints maze)]
                        (map (fn [[to-ch distance]]
                               [{:from ch :to to-ch} distance])
                             (explore-from (:entries maze) loc))))))

(defn permutations [s]
  (if (empty? s)
    [nil]
    (for [h s
          t (permutations (remove #{h} s))]
      (cons h t))))

(defn compute-path-cost [maze-graph path]
  (loop [cost 0
         current (first path)
         remaining (rest path)]
    (if (empty? remaining)
      cost
      (let [next (first remaining)
            step-cost (maze-graph {:from current :to next})]
        (recur (+ step-cost cost) next (rest remaining))))))

(defn solve [make-paths maze-str]
  (let [maze (parse-maze maze-str)
        paths (make-paths (keys (:waypoints maze)))
        maze-graph (find-maze-graph maze)
        path-costs (map #(compute-path-cost maze-graph %) paths)]
    (apply min path-costs)))

(defn solve-part1 [maze-str]
  (solve
    (fn [waypoints] (map #(cons \0 %) (permutations (remove #{\0} waypoints))))
    maze-str))

(defn part1 []
  (solve-part1 input-string))

(defn solve-part2 [maze-str]
  (solve
    (fn [waypoints] (map #(concat [\0] % [\0]) (permutations (remove #{\0} waypoints))))
    maze-str))

(defn part2 []
  (solve-part2 input-string))
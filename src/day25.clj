(ns day25
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [assembunny :as asm])
  (:use advent-util))

(def input-string (slurp (io/resource "day25_input.txt")))

(def input-prog (asm/parse-program input-string))

(defn test-seed [seed program]
  (let [trace (asm/trace-program program {:a seed})
        to-test (first (drop-while #(< (count (:output %)) 50) trace))]
    (every? #(= [0 1] %) (partition 2 (:output to-test)))))

(defn part1 []
  (first (remove nil? (pmap #(if (test-seed % input-prog) %) (range)))))
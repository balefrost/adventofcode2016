(ns day3
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (slurp (io/resource "day3_input.txt")))

(def input-lines (str/split-lines input-string))

(def part1-triangles (map #(map parse-int (re-seq #"\d+" %)) input-lines))

(defn reinterp-triangles [triangle-chunk]
  (let [[as bs cs] triangle-chunk]
    (map vector as bs cs)))

(def part2-triangles (partition 3 (flatten (map reinterp-triangles (partition 3 part1-triangles)))))

(defn possible-triangle? [sides]
  (let [[longest & shorter] (reverse (sort sides))]
    (> (reduce + shorter) longest)))

(defn part1 []
  (count (filter true? (map possible-triangle? part1-triangles))))

(defn part2 []
  (count (filter true? (map possible-triangle? part2-triangles))))
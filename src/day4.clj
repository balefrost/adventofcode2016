(ns day4
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (slurp (io/resource "day4_input.txt")))

(def input-lines (str/split-lines input-string))

(defn split-room-and-checksum [roomstr]
  (let [[_ encname sector checksum] (re-matches #"^([a-z-]+)-([0-9]+)\[([a-z]+)\]$" roomstr)]
    {:encname encname :sector (parse-int sector) :checksum checksum}))

(def input-roomspecs (map split-room-and-checksum input-lines))

(defn count-distinct [seq]
  (let [grouped (group-by identity seq)]
    (for [[k v] grouped] [k (count v)])))

(defn compare-grouped-element [group1 group2]
  (let [[name1 count1] group1
        [name2 count2] group2
        count-comparison (compare count1 count2)]
    (if (= 0 count-comparison)
      (compare name1 name2)
      (- count-comparison))))

(defn decrypt-character [c sector]
  (if (= \- c)
    \space
    (-> c
        (int)
        (- (int \a))
        (+ sector)
        (mod 26)
        (+ (int \a))
        (char))))

(defn decrypt-room-name [room]
  (str/join (map #(decrypt-character % (:sector room)) (:encname room))))

(defn compute-checksum [room-id]
  (str/join
    (take 5
          (map first
               (sort compare-grouped-element
                     (count-distinct (filter #(not (= % \-)) room-id)))))))

(defn is-valid-room [roomspec]
  (= (compute-checksum (:encname roomspec)) (:checksum roomspec)))

(defn part1 []
  (reduce + (map :sector (filter is-valid-room input-roomspecs))))

(defn part2 []
  (filter #(= (:decname %) "northpole object storage") (map (fn [room] { :room room :decname (decrypt-room-name room) }) input-roomspecs)))
(ns day5
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util)
  (:import (java.security MessageDigest)))

(def prefix "ugkcyxxp")

(defn inputs [] (map #(format "%s%d" prefix %) (range)))

(defn byte-to-int [ byte]
  (if (< byte 0)
    (+ 256 byte)
    byte))

(defn is-candidate-hash-part1? [^bytes hash-bytes]
  (and
    (= 0 (aget hash-bytes 0))
    (= 0 (aget hash-bytes 1))
    (let [hb2 (aget hash-bytes 2)]
      (and
        (< hb2 16)
        (>= hb2 0)))))

(defn is-candidate-hash-part2? [^bytes hash-bytes]
  (and
    (= 0 (aget hash-bytes 0))
    (= 0 (aget hash-bytes 1))
    (let [hb2 (aget hash-bytes 2)]
      (and
        (< hb2 8)
        (>= hb2 0)))))


(defn handle-input-string-part1 [^String input-string]
  (let [hash-bytes (compute-md5 (.getBytes input-string "US-ASCII"))]
    (if (is-candidate-hash-part1? hash-bytes)
      (format "%01x" (mod (aget hash-bytes 2) 16)))))

(defn handle-input-string-part2 [^String input-string]
  (let [hash-bytes (compute-md5 (.getBytes input-string "US-ASCII"))]
    (if (is-candidate-hash-part2? hash-bytes)
      (let [position (mod (byte-to-int (aget hash-bytes 2)) 16)
            char (subs (format "%02x" (aget hash-bytes 3)) 0 1)]
        [position char]))))

(defn part1 []
  (let [results (remove nil? (pmap handle-input-string-part1 (inputs)))]
    (str/join (take 8 results))))

(defn accumulate-result [acc [position char]]
  (if (nth acc position)
    acc
    (let [result (assoc acc position char)]
      (println result position char)
      result)))

(defn part2 []
  (let [results (remove nil? (map handle-input-string-part2 (inputs)))
        initial-value (vec (repeat 8 nil))
        steps (reductions accumulate-result initial-value results)
        reduction-result (first (drop-while #(some nil? %) steps))]
    (str/join reduction-result)))

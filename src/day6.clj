(ns day6
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(def input-string (slurp (io/resource "day6_input.txt")))

(def input-lines (str/split-lines input-string))

(defn accumulate [acc line]
  (map (fn [a l] (update a l #(if (nil? %) 1 (+ % 1)))) acc line))

(defn impl [sort-fn]
  (let [stats (reduce accumulate (repeat 8 {}) input-lines)
        maxes (map #(first (sort-by second sort-fn %)) stats)]
    (str/join (map first maxes))))

(defn part1 []
  (impl (comp - compare)))

(defn part2 []
  (impl compare))

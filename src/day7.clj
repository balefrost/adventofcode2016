(ns day7
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(def input-string (slurp (io/resource "day7_input.txt")))

(def input-lines (str/split-lines input-string))

(defn tokenize-address [address]
  (let [tokens (map rest (re-seq #"([a-z]+)|(?:\[([a-z]+)\])" address))
        outside (filter (comp not nil?) (map first tokens))
        inside (filter (comp not nil?) (map second tokens))]
    {:outside outside :inside inside}))

(defn is-abba
  ([str]
   (let [[a b c d] str]
     (is-abba a b c d)))
  ([a b c d]
   (and (= a d) (= b c) (not (= a b)))))

(defn test-aba [a b c]
  (if (and (= a c) (not (= a b)))
    [a b c]
    nil))

(defn find-abas [str]
  (filter identity (apply map test-aba (map #(drop % str) (range 3)))))

(defn invert-aba [aba]
  (let [[a b _] aba]
    [b a b]))

(defn supports-ssl [address]
  (let [{outside :outside inside :inside} (tokenize-address address)
        abas (mapcat find-abas outside)
        babs (set (mapcat find-abas inside))]
    (some babs (map invert-aba abas))))

(defn has-abba [str]
  (some identity (apply map is-abba (map #(drop % str) (range 4)))))

(defn supports-tls [address]
  (let [{outside :outside inside :inside} (tokenize-address address)]
    (and
      (some has-abba outside)
      (every? (comp not has-abba) inside))))

(defn part1 []
  (count (filter identity (map supports-tls input-lines))))

(defn part2 []
  (count (filter identity (map supports-ssl input-lines))))
(ns day8
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (slurp (io/resource "day8_input.txt")))

(def input-lines (str/split-lines input-string))

(defn make-command [str]
  (let [[name expr] (str/split str #" " 2)]
    (condp = name
      "rect" (let [[_ w h] (re-matches #"(\d+)x(\d+)" expr)]
               {:command :rect
                :w       (parse-int w)
                :h       (parse-int h)})
      "rotate" (let [[rowcol expr] (str/split expr #" " 2)
                     [_ rc offset] (re-matches #".=(\d+) by (\d+)" expr)]
                 (condp = rowcol
                   "row" {:command :rotate-row
                          :row     (parse-int rc)
                          :offset  (parse-int offset)}
                   "column" {:command :rotate-column
                             :column  (parse-int rc)
                             :offset  (parse-int offset)})))))

(def input-commands (map make-command input-lines))

(def board-width 50)
(def board-height 6)

(def initial-board (vec (repeat (* board-width board-height) false)))

(defn board-to-string [board]
  (str/join \newline (for [y (range board-height)]
                       (let [row (subvec board (* y board-width) (* (+ y 1) board-width))]
                         (str/join (map #(if (true? %) \O \.) row))))))


(defn rotate-vector-right [vec offset]
  (let [tail (take-last offset vec)
        head (drop-last offset vec)]
    (concat tail head)))

(defn rotate-indices [board indices offset]
  (let [values (map #(nth board %) indices)
        rotated-values (rotate-vector-right values offset)
        updates (map vector indices rotated-values)]
    (reduce (fn [board [idx v]] (assoc board idx v)) board updates)))


(defmulti handle-command (fn [board cmd] (:command cmd)))
(defmethod handle-command :rect [board cmd]
  (let [indices (for [y (range (:h cmd))
                      x (range (:w cmd))]
                  (+ (* y board-width) x))]
    (reduce #(assoc %1 %2 true) board indices)))
(defmethod handle-command :rotate-row [board cmd]
  (let [indices (range (* (:row cmd) board-width) (* (+ (:row cmd) 1) board-width))]
    (rotate-indices board indices (:offset cmd))))
(defmethod handle-command :rotate-column [board cmd]
  (let [indices (range (:column cmd) (* board-width board-height) board-width)]
    (rotate-indices board indices (:offset cmd))))

(defn main []
  (let [states (reductions handle-command initial-board input-commands)
        final-state (last states)]
    (println (board-to-string final-state))
    (count (filter true? final-state))))
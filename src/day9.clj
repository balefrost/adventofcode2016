(ns day9
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (slurp (io/resource "day9_input.txt")))

(defn extract-marker [input]
  (let [[marker input] (split-with #(not (= \) %)) input)
        [_ width count] (re-matches #"^\((\d+)x(\d+)$" (str/join marker))
        width (parse-int width)
        count (parse-int count)]
    [{:width width :count count} (rest input)]))


(defn decompress-v1 [input]
  (letfn [(do-decompress [input]
            (lazy-seq
              (let [[uncompressed input] (split-with #(not (= \( %)) input)
                    more (if (empty? input)
                           nil
                           (let [[{width :width count :count} input] (extract-marker input)
                                 [to-repeat input] (split-at width input)
                                 repeated (flatten (repeat count to-repeat))]
                             (concat repeated (do-decompress input))))]
                (concat uncompressed more))))]

    (do-decompress input)))

(def decompressed-length)

(defn decompressed-length-at-compression-marker [input acc]
  (if (empty? input)
    acc
    (let [[{width :width count :count} input] (extract-marker input)
          [to-repeat input] (split-at width input)
          child-length (decompressed-length to-repeat 0)]
      (decompressed-length input (+ acc (* count child-length))))))

(defn decompressed-length [input acc]
  (let [[uncompressed input] (split-with #(not (= \( %)) input)]
    (decompressed-length-at-compression-marker input (+ acc (count uncompressed)))))


(defn remove-whitespace [seq]
  (filter #(not (#{\space \newline \tab \return} %)) seq))

(defn part1 []
  (count (decompress-v1 (remove-whitespace input-string))))

(defn part2 []
  (decompressed-length (remove-whitespace input-string) 0))
